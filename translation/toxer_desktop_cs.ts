<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>Appearance</name>
    <message>
        <source>Single-Window layout</source>
        <translation>Rozložení jednoho okna</translation>
    </message>
    <message>
        <source>Views are shown in a single window side by side</source>
        <translation>Zobrazení v jednom okně vedle sebe</translation>
    </message>
    <message>
        <source>Slim Layout</source>
        <translation>Tenké rozložení</translation>
    </message>
    <message>
        <source>Slim layout meant for small screens like in pocket-sized devices.</source>
        <translation>Tenké rozložení je určeno pro malé obrazovky kapesních zařízení.</translation>
    </message>
    <message>
        <source>Base Color</source>
        <translation>Základní barva</translation>
    </message>
    <message>
        <source>Hue</source>
        <translation>Odstín</translation>
    </message>
    <message>
        <source>Saturation</source>
        <translation>Nasycení</translation>
    </message>
    <message>
        <source>Lightness</source>
        <translation>Světlost</translation>
    </message>
    <message>
        <source>Light Theme</source>
        <translation>Světelné téma</translation>
    </message>
    <message>
        <source>Font size: %1 pt</source>
        <translation>Velikost písma: %1 pt</translation>
    </message>
</context>
<context>
    <name>CreateProfile</name>
    <message>
        <source>Enter a Tox alias.</source>
        <translation>Zadejte Tox přezdívku.</translation>
    </message>
    <message>
        <source>Location: %1/%2</source>
        <translation>Umístění: %1/%2</translation>
    </message>
    <message>
        <source>Enter the profile&apos;s password.</source>
        <translation>Zadejte heslo profilu.</translation>
    </message>
    <message>
        <source>Retype the profile&apos;s password.</source>
        <translation>Zopakujte heslo profilu.</translation>
    </message>
    <message>
        <source>Create Profile</source>
        <translation>Vytvořit profil</translation>
    </message>
</context>
<context>
    <name>ImportProfiles</name>
    <message>
        <source>&lt;h2&gt;This part of Toxer is work in progress!&lt;/h2&gt;&lt;p&gt;Want to help out?&lt;/p&gt;&lt;p&gt;Visit &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation>&lt;h2&gt;Tato část Toxeru je ve vývoji!&lt;/h2&gt;&lt;p&gt;Chcete pomoci?&lt;/p&gt;&lt;p&gt;Visit &lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt; pro více informací.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <source>Name: %1</source>
        <translation>Jméno: %1</translation>
    </message>
    <message>
        <source>Status Message: %1</source>
        <translation>Status: %1</translation>
    </message>
    <message>
        <source>Tox-Key:</source>
        <translation>Tox-Klíč:</translation>
    </message>
    <message>
        <source>To Clipboard</source>
        <translation>Do schránky</translation>
    </message>
    <message>
        <source>Remove %1 from friends</source>
        <translation>Odstranit %1 z přátel</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
</context>
<context>
    <name>Invite</name>
    <message>
        <source>Invite a friend to join your Tox network.</source>
        <translation>Pozvěte přítele, aby se připojil k vaší síti Tox.</translation>
    </message>
    <message>
        <source>Enter a Tox address.</source>
        <translation>Zadejte Tox adresu.</translation>
    </message>
    <message>
        <source>Send Tox Invitation</source>
        <translation>Poslat Tox pozvánku</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
</context>
<context>
    <name>MainViewSplit</name>
    <message>
        <source>Copy Tox-Address to Clipboard</source>
        <translation>Zkopírovat Tox adresu do schránky</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <source>Appearance</source>
        <translation>Vzhled</translation>
    </message>
    <message>
        <source>Appearance settings</source>
        <translation>Nastavení vzhledu</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <source>Tox</source>
        <translation>Tox</translation>
    </message>
    <message>
        <source>Tox network settings</source>
        <translation>Nastavení sítě Tox</translation>
    </message>
</context>
<context>
    <name>Profiles</name>
    <message>
        <source>Start Profile</source>
        <translation>Spustit Profil</translation>
    </message>
    <message>
        <source>New Profile</source>
        <translation>Nový Profil</translation>
    </message>
    <message>
        <source>Import Profiles</source>
        <translation>Importovat Profil</translation>
    </message>
</context>
<context>
    <name>SelectProfile</name>
    <message>
        <source>Enter the profile&apos;s password</source>
        <translation>Zadejte heslo profilu</translation>
    </message>
    <message>
        <source>Load Profile</source>
        <translation>Otevřít Profil</translation>
    </message>
</context>
<context>
    <name>Tox</name>
    <message>
        <source>Reset</source>
        <translation>Výchozí</translation>
    </message>
    <message>
        <source>Allow IP6 when available.</source>
        <translation>Povolit IPv6, pokud je k dispozici</translation>
    </message>
    <message>
        <source>Enable UDP for Tox transfer.</source>
        <translation>Povolit UDP pro Tox kominikaci.</translation>
    </message>
    <message>
        <source>Proxy Settings</source>
        <translation>Nastavení Proxy</translation>
    </message>
    <message>
        <source>no proxy</source>
        <translation>Žádná proxy</translation>
    </message>
    <message>
        <source>Do not use a proxy.</source>
        <translation>Nepoužívat proxy</translation>
    </message>
    <message>
        <source>SOCKS proxy for simple socket pipes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HTTP proxy using CONNECT.</source>
        <translation>HTTP proxy pomocí CONNECT.</translation>
    </message>
    <message>
        <source>Proxy Address</source>
        <translation>Adresa Proxy</translation>
    </message>
    <message>
        <source>disabled</source>
        <translation>zakázáno</translation>
    </message>
    <message>
        <source>Proxy Port</source>
        <translation>Port Proxy</translation>
    </message>
</context>
<context>
    <name>View</name>
    <message>
        <source>Info: File transfer has been cancelled.</source>
        <translation>Informace: přenos souboru byl zrušen.</translation>
    </message>
    <message>
        <source>File transfer not supported by Toxer -&gt; rejecting.</source>
        <translation>Toxer nepodporuje přenos souborů - &gt; odmítnout.</translation>
    </message>
    <message>
        <source>Send File</source>
        <translation>Poslat soubor</translation>
    </message>
    <message>
        <source>Type a message...</source>
        <translation>Napište zprávu...</translation>
    </message>
    <message>
        <source>Send a file to %1.</source>
        <translation>Odeslat soubor na %1.</translation>
    </message>
</context>
</TS>
