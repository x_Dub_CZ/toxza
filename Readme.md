# Toxza: A Tox client for Ubuntu Touch

Based on Toxer and ToxerCore, using toxcore.


![! WARNING: EXPERIMENTAL !](webres/experimental.png  "ToxerIsExperimental")

**Lots of features are still not implemented. Feel welcome to discuss and contribute your ideas!**

## Building

Using clickable:

You must clone the c-toxcore library inside the root folder of the project so that clickable can build it.
Is recommended to also checkout it to a stable tag, e.g.: 0.2.12.

```
$ clickable build-libs
$ clickable build
```
Note: For some reason build-libs will fail the first time you try to build it on a clean setup, correctly build the second time, and fail again every time after the second time. So I recommend not touching anything after you got a successfull build.

## Resources:

* Contact:
    * **Tox protocol (a.k.a toxcore)** based questions/suggestions are answered by our competent Tox enthusiasts on IRC channels [#tox-dev](irc://irc.freenode.net/#tox-dev) and [#toktok](irc://irc.freenode.net/#toktok)
    * For feature requests on Toxza, please file an issue or contribute


# License

See source code for details
