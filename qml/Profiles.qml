/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick.Controls 2.12 as QC
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0
import Ubuntu.Components 1.3 as UC

import "animations" as Animations
import "base" as Base
import "controls" as Controls
import "style"

import Toxer 1.0
import Toxer.Settings 1.0

UC.Page {
	id: profilePage
	
	header: UC.PageHeader {
		id: head
		title: "Toxza"
		subtitle: qsTr("Using Toxcore ") + Toxer.toxVersionString()
		
		
		trailingActionBar.actions: [
			UC.Action {
				iconName: "info"
				onTriggered: main_container.push("qrc:///qml/About.qml")
			},
			UC.Action {
				iconName: "settings"
				onTriggered: main_container.push("qrc:///qml/settings/Overview.qml")
			}
		]
	}
	
	Flickable {
		id: flicker
		
		anchors {
			top: head.bottom
			left: parent.left
			right: parent.right
			bottom: parent.bottom
			topMargin: units.gu(1)
			leftMargin: units.gu(2)
			rightMargin: units.gu(2)
		}
		
		contentWidth: width
		contentHeight: Math.max(contentColumn.height, height)
		flickableDirection: Flickable.VerticalFlick
		
		Column {
			id: contentColumn
			anchors.horizontalCenter: parent.horizontalCenter
			spacing: units.gu(5)
			
			UC.Icon {
				id: logo
				anchors.horizontalCenter: parent.horizontalCenter
				width: profilePage.width/3
				height: width
				
				source: Style.icon.banner
				color: Style.color.text
				keyColor: "white"
					
				asynchronous: true
			}
			ListView {
				id: pageSelector
				implicitHeight: contentItem.childrenRect.height
				implicitWidth: contentItem.childrenRect.width
				anchors.horizontalCenter: parent.horizontalCenter
				interactive: false
				orientation: ListView.Horizontal
				spacing: units.gu(3)
				model: ListModel {
					ListElement {
						name: qsTr("Start Profile")
						page: 0
					}
					ListElement {
						name: qsTr("New Profile")
						page: 1
					}
					ListElement {
						name: qsTr("Import Profiles")
						page: 2
					}
				}
				delegate: Controls.Text {
					height: paintedHeight + units.gu(4)
					width: implicitWidth + units.gu(4)
					horizontalAlignment: Qt.AlignHCenter
					verticalAlignment: Qt.AlignVCenter
					text: model.name
					color: ListView.isCurrentItem ? "white" : Style.color.text
					font.weight: Font.Bold

					MouseArea {
						anchors.fill: parent
						onClicked: {
							pageSelector.currentIndex = index
						}
					}
				}
				highlightFollowsCurrentItem: false
				highlight: Item {
					Canvas {
						id: highlighter
						width: pageSelector.currentItem.width
						height: pageSelector.currentItem.height
						onPaint: {
							var spikeWidth = pageSelector.spikeWidth
							var cy = height / 2

							var ctx = getContext("2d")
							ctx.save()

							ctx.beginPath()
							ctx.moveTo(ctx.lineWidth, cy)
							ctx.lineTo(spikeWidth, ctx.lineWidth)
							ctx.lineTo(width - spikeWidth, ctx.lineWidth)
							ctx.lineTo(width, cy)
							ctx.lineTo(width - spikeWidth, height - ctx.lineWidth)
							ctx.lineTo(spikeWidth, height - ctx.lineWidth)
							ctx.lineTo(ctx.lineWidth, cy)
							ctx.strokeStyle = pageSelector.highlightColor
							ctx.stroke()
							ctx.fillStyle = pageSelector.highlightColor
							ctx.fill()

							ctx.restore()
						}
						x: pageSelector.currentItem.x

						Behavior on x { SmoothedAnimation { duration: 180 } }
					}
					Glow {
						id: highlighter_effect
						anchors.fill: highlighter
						radius: 13
						samples: 17
						color: pageSelector.highlightColor
						source: highlighter
					}
					Animations.Pulsing {
						target: highlighter_effect
						properties: "spread"
						from: 0.1
						to: 0.3
						durationIn: 180
						durationOut: 300
						loops: Animation.Infinite
						running: true
					}
				}
				onCurrentIndexChanged: {
					var item = model.get(currentIndex)
					pageSwiper.currentIndex = item.page
				}
				Component.onCompleted: { currentIndex = 0 }

				property real spikeWidth: units.gu(2)
				property color highlightColor: Style.color.contrast
			}
			Animations.Ringing {
				target: logo
				duration: 100
				loops: 12
				running: true
			}
			
			QC.SwipeView {
				id: pageSwiper
				width: parent.width
				
				anchors.margins: units.gu(2)
				spacing: units.gu(6)
				
				interactive: false
				
				SelectProfile {}
				CreateProfile {}
				ImportProfiles {}
			}
		}
	}
}
