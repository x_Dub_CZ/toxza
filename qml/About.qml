/* Copyright 2020 Emanuele Sorce
 * 
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */

import QtQuick.Controls 2.12
import QtQuick 2.9
import QtQuick.Layouts 1.3

import Ubuntu.Components 1.3 as UC

UC.Page {
	header: UC.PageHeader {
		id: aboutheader
		width: parent.width
		title: qsTr("About")
	}
	
	Flickable {
		id: flickable
		anchors.fill: parent
		contentHeight:  layout.height + units.dp(80)
		contentWidth: parent.width
		ScrollBar.vertical: ScrollBar { }
		
		Column {
			id: layout

			spacing: units.dp(35)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.right: parent.right
			width: parent.width
			
			Item {
				height: units.dp(30)
				width: 1
			}
			
			Image {
				anchors.horizontalCenter: parent.horizontalCenter
				height: width
				width: Math.min(parent.width/2, parent.height/3)
				source: Qt.resolvedUrl("qrc:///res/images/logo.svg")
			}
			
			Label {
				anchors.horizontalCenter: parent.horizontalCenter
				width: parent.width * 4 / 5
				font.bold: true
				horizontalAlignment: Text.AlignHCenter
				wrapMode: Text.WordWrap
				text: "Toxza"
			}
			
			Label {
				anchors.horizontalCenter: parent.horizontalCenter
				width: parent.width * 4 / 5
				wrapMode: Text.WordWrap
				horizontalAlignment: Text.AlignHCenter
				text: qsTr("Toxza is an open source Tox client for Ubuntu Touch.<br>Community is what makes this app possible, so pull requests, translations, feedback and donations are very appreciated :)<br>This app Is a fork of the Toxer-desktop client by Toxer team, Thanks!<br>This app stands on the shoulder of various Open Source projects, see source code for licensing details");
			}
			
			Label {
				anchors.horizontalCenter: parent.horizontalCenter
				width: parent.width * 4 / 5
				wrapMode: Text.WordWrap
				horizontalAlignment: Text.AlignHCenter
				text: qsTr("A big thanks to the Tox Project")
			}
			
			Button {
				anchors.horizontalCenter: parent.horizontalCenter
				text: qsTr("❤Donate❤")
				highlighted: true
				width: parent.width * 4 / 5
				onClicked: Qt.openUrlExternally("https://paypal.me/emanuele42");
			}
			
			Button {
				anchors.horizontalCenter: parent.horizontalCenter
				text: qsTr("See source on Gitlab")
				highlighted: false
				width: parent.width * 4 / 5
				onClicked: Qt.openUrlExternally("https://gitlab.com/TronFortyTwo/toxza");
			}
			
			Button {
				anchors.horizontalCenter: parent.horizontalCenter
				text: qsTr("Report bug or feature request")
				highlighted: true
				width: parent.width * 4 / 5
				onClicked: Qt.openUrlExternally("https://gitlab.com/TronFortyTwo/toxza/-/issues");
			}
			/*
			Label {
				anchors.horizontalCenter: parent.horizontalCenter
				width: parent.width * 4 / 5
				wrapMode: Text.WordWrap
				horizontalAlignment: Text.AlignHCenter
				text: "Copyright (C) 2021 Emanuele Sorce (emanuele.sorce@hotmail.com)"
			}
			*/
		}
	}
}
