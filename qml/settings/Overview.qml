/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick 2.12
import QtQuick.Layouts 1.12
import Ubuntu.Components 1.3

import "../base" as Base
import "../controls" as Controls

Base.Page {
	id: root

	//ListModel {
		//id: menuModel
	
		//ListElement { caption: qsTr("Appearance"); description: qsTr("Appearance settings"); page: "Appearance.qml" }
		//ListElement { caption: qsTr("Tox"); description: qsTr("Tox network settings"); page: "Tox.qml" }
	//}
  
	header: PageHeader {
		id: headerItem
		title: qsTr("Settings")
		extension: Sections {
			anchors {
				left: parent.left
				leftMargin: units.gu(2)
				bottom: parent.bottom
			}
			actions: [
				// nothing here
// 				Action {
// 					text: qsTr("Appearance")
// 					onTriggered: settingsPage.source = "Appearance.qml"
// 				},
				Action {
					text: qsTr("Tox")
					onTriggered: settingsPage.source = "Tox.qml"
				}
			]
		}
	}
	
	Loader {
		id: settingsPage
		anchors {
			bottom: parent.bottom
			left: parent.left
			right: parent.right
			top: headerItem.bottom
			margins: units.gu(3)
		}
	}
}
