/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick 2.12
import QtQuick.Layouts 1.12
import Ubuntu.Components 1.3

import Toxer 1.0

import "../base" as Base
import "../controls" as Controls
import "../style"

Base.Page {
	id: root

	header: PageHeader {
		id: headerItem
		title: qsTr("Invite")
	}

	ToxProfileQuery { id: tpq }
	Column {
		anchors {
			top: headerItem.bottom
			bottom: parent.bottom
			left: parent.left
			right: parent.right
			margins: units.gu(3)
		}
		spacing: units.gu(3)
		
		Controls.Text {
			width: parent.width
			text: qsTr("Invite a friend to join your Tox network")
		}
		Row {
			id: row_addr
			width: parent.width
			spacing: units.gu(2)
			
			Rectangle {
				anchors.verticalCenter: parent.verticalCenter
				width: tav_indicator.width + units.gu(2)
				height: txtToxAddr.height
				color: txtToxAddr.acceptableInput ? Style.color.valid
                                          : Style.color.nomatch
				Controls.Text {
					id: tav_indicator
					anchors.centerIn: parent
					text: txtToxAddr.condition ? qsTr("VALID") : qsTr("INVALID")
				}
			}
			Controls.TextField {
				id: txtToxAddr
				width: row_addr.width - tav_indicator.width - row_addr.spacing
				placeholderText: qsTr("Enter a Tox address.")
				validator: ToxAddressValidator { id: tav }
				focus: true
			}
		}
		Controls.Text {
			id: error_text
			text: "Invitation failed for some reason"
			color: "red"
			width: parent.width
			visible: false
		}
		Controls.FlatButton {
			id: btnInvite
			anchors.horizontalCenter: parent.horizontalCenter
			Accessible.defaultButton: true
			enabled: txtToxAddr.text && txtToxAddr.acceptableInput
			text: qsTr("Send Tox Invitation")
			onClicked: {
				var result = tpq.inviteFriend(txtToxAddr.text, "Please add me as Tox friend.\n\nRegards,\n" + tpq.userName());
				if(result)
					main_container.pop()
				else
					error_text.visible = true
			}
		}
	}
}
