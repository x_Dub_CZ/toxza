import QtQuick 2.12
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.12 as QC
import QtQuick.Layouts 1.3

import "animations"
import "controls" as Controls
import "style"

Item {
  id: root
  anchors.leftMargin: units.gu(2)
  anchors.rightMargin: units.gu(2)

  Controls.Text {
    id: txt
    width: root.width / 2
    anchors.centerIn: parent
    anchors.margins: 15
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter
    wrapMode: Text.WordWrap
    textFormat: TextEdit.RichText
    text: qsTr("This part is work in progress!")
  }

}
