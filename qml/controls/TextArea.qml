import QtQuick 2.12
import Ubuntu.Components 1.3 as UC
import QtQuick.Controls 2.12 as QC

import "../style"

UC.TextArea {
	//activeFocusOnPress: true
	autoSize: true
	selectByMouse: true
}
