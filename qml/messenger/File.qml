/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0

import Toxer 1.0

import "../controls" as Controls
import "../style"

RowLayout {
  id: root

  readonly property bool isMe: pk === toxProfile.publicKeyStr()

  Controls.Text {
    id: msg_time
    Layout.alignment: Qt.AlignTop
    text: Qt.formatTime(new Date(ts), "hh:mm:ss")
  }
  Rectangle {
    id: msg_container
    Layout.fillWidth: true
    implicitHeight: msg_layout.height
    color: root.isMe ? "#5E97EB" : "#424954"
    radius: 4

    Row {
      id: msg_layout
      layoutDirection: root.isMe ? Qt.LeftToRight : Qt.RightToLeft

      Item {
        id: pic_container
        height: units.gu(15)
        width: height

        Image {
          id: pic
          anchors.fill: parent
          visible: false
          sourceSize: Qt.size(width, height)
          source: {
            var url = Toxer.avatarsUrl() + "/" + pk.toUpperCase() + ".png"
            return Toxer.exists(url) ? url : Style.icon.noAvatar
          }
        }
        Rectangle {
          id: pic_mask
          anchors.fill: pic
          color: "blue"
          radius: width / 2
          clip: true
          visible: false
        }
        OpacityMask {
          anchors.fill: pic_mask
          source: pic
          maskSource: pic_mask
        }
      }
      Controls.Text {
        id: msg_text
        padding: 4
        text: message
        color: "#F6F6F9"
        width: msg_container.width - pic_container.width
        maximumLineCount: 50
        wrapMode: Text.WordWrap
        elide: Text.ElideNone
      }
    }
  }
}
