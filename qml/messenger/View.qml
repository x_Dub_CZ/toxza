/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.12
import Ubuntu.Components 1.3 as UC

import Toxer 1.0

import "../base" as Base
import "../controls" as Controls
import "../friend" as Friend
import "../style"

Base.Page {
	id: root
	anchors.fill: parent
	
	ToxProfileQuery {
		id: toxProfile
	}
  
	header: UC.PageHeader {
		id: headerItem
		title: my_friend.name
		
		trailingActionBar.actions: [
			UC.Action {
				iconName: "info"
				onTriggered: main_container.push("qrc:///qml/friend/Info.qml", { friendNo: root.friendNo, parent: root })
			}
		]
	}
  
	property int friendNo

	function sendMessage() {
		var message = txt_message_input.text
		if (message) {
			// TODO: toxcor API specifies a max message length - make sure is respected!
			if( my_friend.sendMessage(message) ) {
				messageModel.addMessageItem(toxProfile.publicKeyStr().toUpperCase(), message)
				txt_message_input.text = ""
			}
		}
		messageBox.positionViewAtEnd()
	}
	
	function addMessageItem(p, m) {
		messageModel.addMessageItem(p, m)
		if(!visible)
			unreadMessages++
	}
	// reload the chat
	function reload() {
		reloadMessageModel.start()
	}
	onVisibleChanged: {
		if(visible) {
			reload()
		}
	}
	
	ListModel {
		id: messageModel

		function addMessageItem(pk, message) {
			var ts = new Date().getTime()
			append({ pk: pk, ts: ts, message: message })
			chatDatabase.addMessage(
				toxProfile.publicKeyStr(),
				chatDatabase.chat_type_private,
				my_friend.pkStr(),
				pk,
				chatDatabase.msg_type_textmessage,
				message,
				ts,
				chatDatabase.msg_status_unsupported
			)
		}
		
		function reload() {
			clear()
			var chat = chatDatabase.getChat(toxProfile.publicKeyStr(), my_friend.pkStr())
			
			for(var i=0; i<chat.rows.length; i++){
				messageModel.append({
					pk: chat.rows.item(i).cfrom,
					ts: chat.rows.item(i).timestamp,
					message: chat.rows.item(i).content
				})
			}
			messageBox.positionViewAtEnd()
		}
	}

	Timer {
		id: reloadMessageModel
		
		repeat: false
		running: false
		triggeredOnStart: false
		interval: 50
		
		onTriggered: {
			messageModel.reload()
		}
	}
	
	Item {
		anchors {
			top: headerItem.bottom
			bottom: parent.bottom
			left: parent.left
			right: parent.right
			bottomMargin: units.gu(1)
		}
		width: parent.width
		onHeightChanged: messageBox.positionViewAtEnd()
		
		ListView {
			id: messageBox
			
			width: parent.width
			anchors.top: parent.top
			anchors.bottomMargin: units.gu(1)
			anchors.bottom: row_input.top
			anchors.horizontalCenter: parent.horizontalCenter
			
			spacing: units.gu(1)
			clip: true
			model: messageModel
			delegate: Message {
				width: messageBox.width
				anchors.leftMargin: units.gu(1)
				anchors.rightMargin: units.gu(1)
			}
		}
		
		Row {
			id: row_input
			width: parent.width
			height: units.gu(5)
			anchors.bottom: parent.bottom
			anchors.horizontalCenter: parent.horizontalCenter
			spacing: 0
			clip: true
			Item {
				id: statusIcon
				width: height
				height: parent.height
				UC.Icon {
					source: my_friend_icon
					anchors.centerIn: parent
					width: parent.width * 0.5
					height: parent.height * 0.5
					asynchronous: true
				}
			}
			/*
			Controls.RoundButton {
				id: btn_send_file
				Accessible.defaultButton: true
				enabled: true//!fileDialog.visible
				text: qsTr("Send File")
				height: parent.height
				//onClicked: { fileDialog.visible = true; }
			}*/
			ScrollView {
				//id: input_view
				height: parent.height
				width: parent.width - btn_send_message.width - statusIcon.width
				TextArea {
					id: txt_message_input
					width: parent.width
					height: row_input.height
					placeholderText: qsTr("Type a message...")
					wrapMode: TextEdit.WordWrap
				}
			}
			UC.Button {
				id: btn_send_message
				height: parent.height
				width: height * 1.5
				enabled: my_friend.isOnline
				iconName: enabled ? "send" : "weather-severe-alert-symbolic"
				color: "transparent"
				onClicked: {
					if(txt_message_input.text)
						root.sendMessage();
				}
			}
		}
	}

	// TODO: file support
	/*
	FileDialog {
		id: fileDialog
		title: qsTr("Send a file to %1.").arg(my_friend.name)
		selectExisting: true
		folder: shortcuts.home
		onAccepted: my_friend.sendFile(fileUrl)
	}*/
}
