/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

pragma Singleton
import QtQuick 2.12

import Toxer 1.0
import Toxer.Types 1.0

QtObject {
  readonly property QtObject icon: QtObject {
    property url noAvatar: "image://theme/contact"
    property url offline: "qrc:/res/images/dot_offline.svg"
    property url online: "qrc:/res/images/dot_online.svg"
    property url away: "qrc:/res/images/dot_away.svg"
    property url busy: "qrc:/res/images/dot_busy.svg"
	property url banner: "qrc:/res/images/login_logo.svg"

    function availability(isOnline, status) {
      if (isOnline) {
        switch (status) {
        case ToxTypes.Unknown:
        case ToxTypes.Away: return away
        case ToxTypes.Busy: return busy
        case ToxTypes.Ready: return online
        }
        console.warn("No icon for unknown availability " + status)
        return away
      }
      return offline
    }
  }

  readonly property QtObject color: QtObject {
    property color base: theme.palette.normal.base
    property color contrast: "#0070e4"	// this is a nice blue used for the icon
    property color nomatch: theme.palette.normal.negative
    property color valid: theme.palette.normal.positive
    property color text: theme.palette.normal.baseText
    property color hover: {
      var c = base
      return Qt.hsla(c.hslHue + 0.2, c.hslSaturation,
                     c.hslLightness + (c.hslLightness <= 0.5 ? 0.1 : -0.1))
    }

    readonly property QtObject status: QtObject {
      property color disabled: "gray"
      property color enabled: "#80ff80"
      property color pending: "#bbbb80"
      property color active: "#ff8080"
    }

    readonly property QtObject user: QtObject {
      property color offline: "gray"
      property color online: "#80ff80"
      property color away: "#bbbb80"
      property color busy: "#ff8080"
    }
  }
}
