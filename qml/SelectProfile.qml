/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

import QtQuick 2.12
import QtQuick.Controls 2.12 as QC
import QtQuick.Layouts 1.3

import Ubuntu.Components 1.3 as UC

import "controls" as Controls

ColumnLayout {
	id: root
	anchors.leftMargin: units.gu(2)
	anchors.rightMargin: units.gu(2)
	spacing: units.gu(2)

	function startToxSession() {
		var result = Toxer.activateProfile(profileSelector.currentText, txtPassword.text)
		if(result)
			loading.running = true
		else
			error_text.visible = true
	}

	function reloadList() {
		profileSelector.model = Toxer.availableProfiles()
	}
	
	Timer {
		interval: 1000
		repeat: true
		running: true
		onTriggered: {
			reloadList()
		}
	}

	QC.ComboBox {
		id: profileSelector
		Layout.fillWidth: true
		model: Toxer.availableProfiles()
	}
	Controls.TextField {
		id: txtPassword
		Layout.fillWidth: true
		echoMode: TextInput.Password
		placeholderText: qsTr("Enter the profile's password")
		onAccepted: {
			error_text.visible = false
		}
	}
	Controls.FlatButton {
		id: btnStart
		Layout.fillWidth: true
		padding: units.gu(3)
		focus: true
		text: qsTr("Load Profile")
		enabled: txtPassword.length > 0
		onClicked: {
			root.startToxSession();
		}
	}
	Controls.Text {
		id: error_text
		color: "red"
		text: "Wrong password or other loading problem"
		visible: false
		Layout.fillWidth: true
	}
	UC.ActivityIndicator {
		Layout.alignment: Qt.AlignHCenter
		id: loading
		width: units.gu(5)
		height: units.gu(5)
	}
}
